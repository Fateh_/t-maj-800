import React, { Component } from 'react';
import { Dropdown } from 'react-bootstrap';
import { Redirect, Link } from 'react-router-dom';

class Navbar extends Component {

  state = {
    isLogged: true
  }

  Button = (props) => {
    if (props.type === "CHAINE") {
      return (
        <ul className="navbar-nav navbar-nav-left header-links">
          <li className="nav-item d-none d-md-flex">
            <a href="!#" onClick={evt => evt.preventDefault()} className="nav-link">Schedule <span className="badge badge-primary ml-1">New</span>
            </a>
          </li>
        </ul>
      )
    }
    else {
      return (
        <ul className="navbar-nav navbar-nav-left header-links">
          <li className="nav-item d-none d-md-flex">
            <p>lala</p>
          </li>
        </ul>
      )
    }
  }

  NavProfileButton = () => {
    return (
      <ul className="navbar-nav navbar-nav-right ml-lg-auto">
        <li className="nav-item  nav-profile border-0">
          <Dropdown alignRight>
            <Dropdown.Toggle className="nav-link count-indicator bg-transparent">
              <span className="profile-text">{localStorage.getItem("username")}</span>
              <img className="img-xs rounded-circle" src={require("../../assets/images/faces-clipart/pic-1.png")} alt="Profile" />
            </Dropdown.Toggle>
            <Dropdown.Menu className="preview-list navbar-dropdown pb-3">
              <Dropdown.Item className="dropdown-item p-0 preview-item d-flex align-items-center border-bottom" href="!#" onClick={evt => evt.preventDefault()}>
                <div className="d-flex">
                  <div className="py-3 px-4 d-flex align-items-center justify-content-center">
                    <Link to="/profile"><i className="mdi mdi-account-outline mr-0"></i></Link>
                  </div>
                </div>
              </Dropdown.Item>
              <Dropdown.Item className="dropdown-item preview-item d-flex align-items-center border-0 mt-2" onClick={this.onSignOut}>
                Se déconnecter
                  </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </li>
      </ul>
    )
  }

  onSignOut = () => {
    localStorage.clear('token')
    localStorage.clear('username')
    this.setState({isLogged: false})
  }


  render() {
    if (!this.state.isLogged) {
      return <Redirect to="/login"/>
    }
    return (
      <nav className="navbar col-lg-12 col-12 p-lg-0 fixed-top d-flex flex-row">
        <div className="navbar-menu-wrapper d-flex align-items-center justify-content-between">
          <button className="navbar-toggler navbar-toggler align-self-center" type="button" onClick={() => document.body.classList.toggle('sidebar-icon-only')}>
            <i className="mdi mdi-menu"></i>
          </button>
          <this.NavProfileButton />
          <button className="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" onClick={this.toggleOffcanvas}>
            <span className="mdi mdi-menu"></span>
          </button>
        </div>
      </nav>
    );
  }
}

export default Navbar;
