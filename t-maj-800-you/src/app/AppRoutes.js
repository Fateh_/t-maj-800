import React, { Component, Suspense, lazy, Fragment } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Spinner from '../app/shared/Spinner';

const Login = lazy(() => import('./user-pages/Login'))
const Register = lazy(() => import('./user-pages/Register'))
const Profile = lazy(() => import('./user-pages/Profile'))
const BlankPage = lazy(() => import('./user-pages/BlankPage'))


class AppRoutes extends Component {

  state = {
    isLogged: false
  }

  render() {
    return (
      <Suspense fallback={<Spinner />}>
        <Switch>
          <Route path="/login" component={Login} />
          <Route path="/register" component={Register} />
          {localStorage.getItem('token') != null? 
          <Fragment>
            <Route exact path="/" component={BlankPage} /> 
            <Route path="/profile" component={Profile} />
          </Fragment>: <Redirect to="/login"/>}
        </Switch>
      </Suspense>
    );
  }
}

export default AppRoutes;