import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import { Form } from 'react-bootstrap'
import axios from 'axios'

export class Login extends Component {

  state = {
    username: "",
    password: "",
    error: false,
    isLogged: false
  }


  onChangeUsername = event => {
    const username = event.target.value
    this.setState({
      username: username
    })
  }

  onChangePassword = event => {
    const password = event.target.value
    this.setState({
      password: password
    })
  }

  onClickLogin = () => {
    axios.post("http://localhost:8078/login", {
      "username": this.state.username,
      "password": this.state.password
    }).then(response => {
      if (response.status === 200) {
        localStorage.setItem('token', response.headers.authorization)
        localStorage.setItem('username', this.state.username)
        console.log(response.headers.authorization)
        this.setState({ isLogged: true })
      } else {
        this.setState({ error: true })
        console.log(response)
      }
    }).catch(e => {
      this.setState({ error: true })
      console.log(e)
    })
  }



  render() {
    if (this.state.isLogged) {
      return <Redirect to="/"/>
    }
    return (
      <div>
        <div className="d-flex align-items-center auth px-0">
          <div className="row w-100 mx-0">
            <div className="col-lg-4 mx-auto">
              <div className="auth-form-light text-left py-5 px-4 px-sm-5">
                <h4>Bienvenue!</h4>
                <h6 className="font-weight-light">Connectez-vous pour continuer.</h6>
                <Form className="pt-3">
                  <Form.Group className="d-flex search-field">
                    <Form.Control
                      type="email"
                      placeholder="E-mail"
                      size="lg"
                      className="h-auto"
                      value={this.state.username}
                      onChange={this.onChangeUsername} />
                  </Form.Group>
                  <Form.Group className="d-flex search-field">
                    <Form.Control
                      type="password"
                      placeholder="Mot de passe"
                      size="lg"
                      className="h-auto"
                      value={this.state.password}
                      onChange={this.onChangePassword} />
                  </Form.Group>
                  <div className="mt-3">
                    <div className="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" onClick={this.onClickLogin}>Se Connecter</div>
                  </div>
                  <div className="text-center mt-4 font-weight-light">
                    Don't have an account? <Link to="register" className="text-primary">Create</Link>
                  </div>
                  {this.state.error&&
                  <div className="text-center mt-4 font-weight-light">Le username ou le mot de passe indiqués ne sont pas correctes</div>}
                </Form>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Login
