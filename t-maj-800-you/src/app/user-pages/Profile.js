import React, { Component } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router-dom'

export class Profile extends Component {
    state = {
        email: "",
        password: "",
        isSubmited: false
    }

    componentDidMount() {
        axios.get("http://localhost:8078/users/" + localStorage.getItem("username"), {
            headers: {
                "authorization": localStorage.getItem("token")
            }
        }).then(response => {
            if (response.status === 200) {
                this.setState({
                    isSubmited: true,
                    email: response.email,
                    password: ""
                })
            } else {
                console.log(response)
            }
            console.log(response)
        }).catch(e => {
            console.log(e)
        })
    }

    onChangeEmail = event => {
        const email = event.target.value
        this.setState(
            {
                email: email
            }
        )
    }

    onChangePassword = event => {
        const password = event.target.value
        this.setState(
            {
                password: password
            }
        )
    }

    onClickSubmited = () => {
        axios.put("http://localhost:8078/users" + localStorage.getItem("username"), {
            headers: {
                "authorization": localStorage.getItem("token")
            },
            "email": this.state.email,
            "password": this.state.password
        }).then(response => {
            if (response.status === 200) {
                this.setState({ isSubmited: true })
            } else {
                this.setState({ isSubmited: false })
                console.log(response)
            }
        }).catch(e => {
            this.setState({ isSubmited: false })
            console.log(e)
        })
    }


    render() {
        if (this.state.isSubmited) {
            return <Redirect to="/"/>
        }
        return (
            <div>
                <div className="d-flex align-items-center auth px-0">
                    <div className="row w-100 mx-0">
                        <div className="col-lg-4 mx-auto">
                            <div className="auth-form-light text-left py-5 px-4 px-sm-5">
                                <form className="pt-3">
                                    <div className="form-group">
                                        <input type="email" className="form-control form-control-lg" id="user_email" placeholder="E-mail" value={this.state.email} onChange={this.onChangeEmail} />
                                    </div>
                                    <div className="form-group">
                                        <input type="password" className="form-control form-control-lg" id="user_password" placeholder="Mot de passe" value={this.state.password} onChange={this.onChangePassword} />
                                    </div>
                                    <div className="mt-3">
                                        <div className="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" onClick={this.onClickSubmited}>Enregistrer les modifications</div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Profile
