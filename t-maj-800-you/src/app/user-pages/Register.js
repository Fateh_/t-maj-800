import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'
import axios from 'axios'

export class Register extends Component {

  state = {
    username: "",
    email: "",
    password: "",
    role: "",
    error: "",
    isRegistred: false
  }

  onChangeEmail = event => {
    const email = event.target.value
    this.setState(
      {
        email: email
      }
    )
  }

  onChangePassword = event => {
    const password = event.target.value
    this.setState(
      {
        password: password
      }
    )
  }

  onChangeUsername = event => {
    const username = event.target.value
    this.setState(
      {
        username: username
      }
    )
  }

  onChangeRole = event => {
    const role = event.target.value
    if (role === "Chaine de télévision") {
      this.setState(
        {
          role: "Chaine"
        }
      )
    } else {
      this.setState({
        role: "CSA"
      })
    }
  }

  onClickRegister = () => {
    axios.post("http://localhost:8078/users/sign-up", {
      "username": this.state.username,
      "email": this.state.email,
      "role": this.state.role,
      "password": this.state.password
    }).then(response => {
      if (response.status === 200) {
        this.setState({ isRegistred: true })
      } else {
        this.setState({ error: true })
        console.log(response)
      }
    }).catch(e => {
      this.setState({ error: true })
      console.log(e)
    })
  }



  render() {
    if (this.state.isRegistred) {
      return <Redirect to="/login" /> 
    }
    return (
      <div>
        <div className="d-flex align-items-center auth px-0">
          <div className="row w-100 mx-0">
            <div className="col-lg-4 mx-auto">
              <div className="auth-form-light text-left py-5 px-4 px-sm-5">
                <h4>Nouveau ici?</h4>
                <h6 className="font-weight-light">La création de compte est facile. Il suffit de remplir les champs suivants!</h6>
                <form className="pt-3">
                  <div className="form-group">
                    <input type="text" className="form-control form-control-lg" id="user_username" placeholder="Username" onChange={this.onChangeUsername}/>
                  </div>
                  <div className="form-group">
                    <input type="email" className="form-control form-control-lg" id="user_email" placeholder="E-mail" onChange={this.onChangeEmail}/>
                  </div>
                  <div className="form-group">
                    <select className="form-control form-control-lg" id="user_role" onChange={this.onChangeRole}>
                      <option>Chaine de télévision</option>
                      <option>CSA</option>
                    </select>
                  </div>
                  <div className="form-group">
                    <input type="password" className="form-control form-control-lg" id="user_password" placeholder="Mot de passe" onChange={this.onChangePassword}/>
                  </div>
                  <div className="mt-3">
                    <div className="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" onClick={this.onClickRegister}>SIGN UP</div>
                  </div>
                  <div className="text-center mt-4 font-weight-light">
                    Already have an account? <Link to="/login" className="text-primary">Login</Link>
                  </div>
                  {this.state.error&&
                  <div className="text-center mt-4 font-weight-light">{this.state.error}</div>}
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Register
