package com.epitech.auth.controllers;


import com.epitech.auth.exception.NotExistingException;
import com.epitech.auth.exception.ResourceNotFoundException;
import com.epitech.auth.models.ApplicationUser;
import com.epitech.auth.models.Diffusion;
import com.epitech.auth.models.Programme;
import com.epitech.auth.repository.DiffusionRepository;
import com.epitech.auth.repository.ProgrammeRepository;
import com.epitech.auth.repository.UserRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

// Autoriser la passation des requêtes
@CrossOrigin(origins = "http://localhost:3000")
@RestController
@Api(value = "Controller used for all Diffusion management")
// Classe du user controller
public class DiffusionController {

    // Déclaration des différents repositories

    @Autowired
    private DiffusionRepository diffusionRepository;

    @Autowired
    private ProgrammeRepository programmeRepository;

    @Autowired
    private UserRepository userRepository;

    // Route Get pour retourner la liste des diffusions par le user id
    @ApiOperation(value="Returns a List of all Diffusions by Channel ID", response= Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 403, message = "Access Denied"),
            @ApiResponse(code = 404, message = "Diffusion not found with id : {id}")
    })
    @GetMapping(value ="/user/{user_id}/diffusion")
    public Page<Diffusion> getDiffusionByUser(@PathVariable(value= "user_id")Long user_id, Pageable pageable){
        ApplicationUser user = userRepository.findById(user_id).orElseThrow(() -> new ResourceNotFoundException("User", "id", user_id));
        return diffusionRepository.findByUser(user, pageable);
    }

    // Route Get pour retourner laliste des diffusions par le nom du user
    @ApiOperation(value="Returns a List of all Diffusions by Channel name", response= Page.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 403, message = "Access Denied"),
            @ApiResponse(code = 500, message = "User with name {name} doesnt exist")
    })
    @GetMapping(value ="/user/name/{username}/diffusion")
    public Page<Diffusion> getDiffusionByUserName(@PathVariable(value= "username")String username, Pageable pageable){
        ApplicationUser user = userRepository.findByUsername(username);
        if (user == null){
            throw new NotExistingException("User", username);
        }
        return diffusionRepository.findByUser(user, pageable);
    }

    // Méthode POST pour créer une diffusion en reseignant le nom du programme, le username ainsi que la date de diffusion
    @ApiOperation(value="Creates a diffusion by linking a Program to a channel, a date and a price", response= Diffusion.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 403, message = "Access Denied"),
            @ApiResponse(code = 500, message = "{Resource} with name {name} doesnt exist")
    })
    @PostMapping(value="/user/{username}/{programme_name}/{date}")
    public Diffusion createDiffusion(@PathVariable(value = "username")String username,
                                             @PathVariable(value="programme_name")String programme_name,
                                                @PathVariable(value="date")String date) throws NotExistingException{
        Programme programme = programmeRepository.findByName(programme_name);
        if (programme == null){
            throw new NotExistingException("Programme", programme_name);
        }
        ApplicationUser user = userRepository.findByUsername(username);
        if (user == null){
            throw new NotExistingException("User", username);
        }
        Diffusion diffusion = new Diffusion();
        diffusion.setProgramme(programme);
        diffusion.setChaine(user);
        int prix=0;
        if (programme.getType() == "Film"){
            if (programme.getAnnee() >= 2009){
                prix = 100000 + 100000*programme.getNote().intValue();
            }else{
                prix = 100000*programme.getNote().intValue();
            }
        }else if(programme.getType() == "Ancienne") {
            prix = 3000 + (programme.getNote().intValue() / 10) * 4000;
        }else if (programme.getType() == "Recente"){
            prix = 1000 + programme.getNote().intValue()*1000;
        }else if (programme.getType() == "Inedite" ){
            prix = 20000 + (programme.getNote().intValue() / 10) * 50000;
        }else{
            prix = 30000 + (programme.getNote().intValue() / 10) *30000;
        }
        diffusion.setPrix(new Long (prix));
        diffusion.setDate(date);
        return diffusionRepository.save(diffusion);
    }

    // Méthode PUT pour mettre à jour une diffusion
    @ApiOperation(value="Updates a diffusion by its id and the id of the Channel it is linked to", response= Diffusion.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 403, message = "Access Denied"),
            @ApiResponse(code = 500, message = "User with name {name} doesnt exist")
    })
    @PutMapping(value="/user/{username}/diffusion/{diffusion_id}")
    public Diffusion changeDiffusion(@PathVariable(value ="username")String username,
                                     @PathVariable(value ="diffusion_id")Long diffusion_id,
                                     @Valid @RequestBody Diffusion diffusion){
        ApplicationUser user = userRepository.findByUsername(username);
        if (user == null){
            throw new NotExistingException("User", username);
        }
        return diffusionRepository.findById(diffusion_id).map(diffusion1 -> {
            diffusion1.setChaine(diffusion.getChaine());
            diffusion1.setDate(diffusion.getDate());
            diffusion1.setProgramme(diffusion.getProgramme());
            diffusion.setPrix(diffusion.getPrix());
            return diffusionRepository.save(diffusion1);
        }).orElseThrow(() -> new ResourceNotFoundException("Diffusion", "id", diffusion_id));
    }


    // Méthode DELETE pour supprimer une diffusion
    @ApiOperation(value="Deletes a diffusion by its id and returns an Ok Status", response= ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 403, message = "Access Denied"),
            @ApiResponse(code = 500, message = "Diffusion with name {name} doesnt exist")
    })
    @DeleteMapping(value="/user/{username}/diffusion/{diffusion_id}")
    public ResponseEntity<?> deleteDiffusion(@PathVariable(value="username")String username,
                                             @PathVariable(value="diffusion_id")Long diffusion_id){
        ApplicationUser user = userRepository.findByUsername(username);
        if (user == null){
            throw new NotExistingException("User", username);
        }
        return diffusionRepository.findByIdAndUser(diffusion_id, user).map(diffusion -> {
            diffusionRepository.delete(diffusion);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("Diffusion", "id", diffusion_id));
    }

}
