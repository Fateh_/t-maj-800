package com.epitech.auth.controllers;

import com.epitech.auth.exception.BadRequestException;
import com.epitech.auth.exception.NotExistingException;
import com.epitech.auth.exception.ResourceNotFoundException;
import com.epitech.auth.models.Programme;
import com.epitech.auth.repository.ProgrammeRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

// CrossOrigin: pour autoriser la passation des requêtes entre les 2 parties front/back
@CrossOrigin(origins = "http://localhost:3000")
@RestController
@Api(value = "Controller used for all Programme management")
@RequestMapping(value = "/programme")
// Classe du programme controller
public class ProgrammeController {

    // Déclaration du repository pour interagir directement avec la bdd
    @Autowired
    ProgrammeRepository programmeRepository;

    // Méthode GET pour retourner la liste de tous les programmes
    @ApiOperation(value="Returns a List of all Programmes", response= Iterable.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 403, message = "Access Denied")
    })
    @GetMapping(value="/all")
    public Iterable<Programme> getAll(){
        return programmeRepository.findAll();
    }

    // Méthode GET pour retourner un programme par son ID
    @ApiOperation(value="Returns a Programme by its ID", response= Programme.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 403, message = "Access Denied"),
            @ApiResponse(code = 404, message = "Programe not found with id : {id}")
    })
    @GetMapping(value = "/{id}")
    public Programme getProgrammme(@PathVariable(value="id") Long id){
        return programmeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Programme", "id", id));
    }

    // Méthode GET pour récupérer un programme par son nom
    @ApiOperation(value="Returns a Programme by its name", response= Programme.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 403, message = "Access Denied"),
            @ApiResponse(code = 500, message = "Programme with name {name} doesnt exist")
    })
    @GetMapping(value="/name/{programme_name}")
    public Programme getProgrammeByName(@PathVariable(value ="programme_name")String programme_name){
        Programme programme = programmeRepository.findByName(programme_name);
        if (programme == null){
            throw new NotExistingException("Programme",programme_name);
        }
        return programme;
    }

    // Méthode POST pour créer un nouveau programme
    @ApiOperation(value="Creates a new Programme", response= Programme.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 403, message = "Access Denied"),
            @ApiResponse(code = 400, message = "Programme {name} already exists")
    })
    @PostMapping
    public Programme createProgramme(@Valid @RequestBody Programme programme){
        if (programmeRepository.findByName(programme.getName()) != null){
            throw new BadRequestException("Programme",programme.getName());
        }
        return programmeRepository.save(programme);
    }

    // Méthode PUT pour mêttre à jour un programme par son id
    @ApiOperation(value="Updates an existing Programme by its id", response= Programme.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 403, message = "Access Denied"),
            @ApiResponse(code = 404, message = "Programe not found with id : {id}")
    })
    @PutMapping(value ="/{id}")
    public Programme updateProgramme(@PathVariable(value="id") Long id, @Valid @RequestBody Programme programmeDetails){
        Programme programme = programmeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Programme", "id", id));
        programme.setName(programmeDetails.getName());
        programme.setOwner(programmeDetails.getOwner());
        programme.setAnnee(programmeDetails.getAnnee());
        programme.setNote(programmeDetails.getNote());
        programme.setType(programmeDetails.getType());

        return programmeRepository.save(programme);
    }

    // Méthode DELETE pour supprimer un programme en renseignant son ID
    @ApiOperation(value="Deletes an existing Programme by its id", response= Programme.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 403, message = "Access Denied"),
            @ApiResponse(code = 404, message = "Programe not found with id : {id}")
    })
    @DeleteMapping(value ="/{id}")
    public Programme deleteProgramme(@PathVariable(value="id")Long id){
        Programme programme = programmeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Programme", "id", id));
        programmeRepository.delete(programme);

        return programme;
    }

}
