package com.epitech.auth.controllers;

import com.epitech.auth.dto.UserUpdateDto;
import com.epitech.auth.exception.BadRequestException;
import com.epitech.auth.exception.NotExistingException;
import com.epitech.auth.models.ApplicationUser;
import com.epitech.auth.repository.UserRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import javax.validation.Valid;
import java.util.List;

// CrossOrigin pour autoriser la passation des requêtes entre les deux parties front/back
@CrossOrigin(origins = "http://localhost:3000")
@RestController
@Api(value = "Controller used for all Authentification management")
@RequestMapping(value = "/users")
// Classe du user controller
public class UserController {

    // Déclaration des variables 
    private UserRepository applicationUserRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    // Constructeur
    public UserController(UserRepository applicationUserRepository,
                          BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.applicationUserRepository = applicationUserRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    // Méthode POST pour créer un nouveau compte utilisateur
    @ApiOperation(value = "Creates a new application account")
    @PostMapping("/sign-up")
    public void signUp(@RequestBody ApplicationUser user) throws BadRequestException {
        if (applicationUserRepository.findByUsername(user.getUsername()) != null) {
            throw new BadRequestException("User", user.getUsername());
        }
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        applicationUserRepository.save(user);
    }

    // Méthode GET pour retourner la liste de tous les utilisateurs
    @ApiOperation(value = "Returns a List of all Users ", response = List.class)
    @GetMapping("/all")
    public List<ApplicationUser> getAll() {
        return applicationUserRepository.findAll();
    }

    // Méthode GET pour récupérer un user par son username
    @ApiOperation(value = "Finds and returns a User by its username ", response = List.class)
    @GetMapping("/{username}")
    public ApplicationUser getByUsername(@PathVariable(value = "username") String username) {
        ApplicationUser user = applicationUserRepository.findByUsername(username);
        if (user == null) {
            throw new NotExistingException("User", username);
        }
        return user;
    }

    // Méthode PUT pour mêttre à jour un user par son username
    @ApiOperation(value = "Updates a User by its username", response = List.class)
    @PutMapping("/{username}")
    public ApplicationUser changeByUsername(@PathVariable(value = "username") String username,
                                            @Valid @RequestBody UserUpdateDto userUpdateDto) {
        ApplicationUser user = applicationUserRepository.findByUsername(username);
        if (user == null) {
            throw new NotExistingException("User", username);
        }
        if (userUpdateDto.getEmail() != null) {
            user.setEmail(userUpdateDto.getEmail());
        }
        if (userUpdateDto.getPassword() != null){
            user.setPassword(bCryptPasswordEncoder.encode(userUpdateDto.getPassword()));
        }
        return applicationUserRepository.save(user);
    }
}