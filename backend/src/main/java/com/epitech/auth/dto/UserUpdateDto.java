package com.epitech.auth.dto;

// Adapteur du user
public class UserUpdateDto {

    // Déclaration des attributs
    private String email;
    private String password;

    // Constructeur sans paramètres
    public UserUpdateDto() {}

    // Constructeur avec paramètres
    public UserUpdateDto(String password, String email){
        this.email = email;
        this.password = password;
    }

    /* Getters & Setters */

    public String getEmail(){return this.email;}
    public void setEmail(String email){this.email = email;}

    public String getPassword(){return this.email;}
    public void setPassword(String password){this.password = password;}
}
