package com.epitech.auth.repository;

import com.epitech.auth.models.ApplicationUser;
import com.epitech.auth.models.Diffusion;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

// Interface repository diffusion qui interagit directement avec la base de données
@Repository
public interface DiffusionRepository extends JpaRepository<Diffusion, Long> {

    // Méthode pour récupérer une diffusion à partir du user qui diffuse
    Page<Diffusion> findByUser(ApplicationUser user, Pageable pageable);

    // Méthode pour récupérer une diffusion avec son id et son user
    Optional<Diffusion> findByIdAndUser(Long id, ApplicationUser user);

    // Récupérer une diffusion à partir de l'id du programme diffusé
    Page<Diffusion> findByProgrammeId(Long programmeId, Pageable pageable);
}
