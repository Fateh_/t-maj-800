package com.epitech.auth.repository;

import com.epitech.auth.models.Programme;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

// Interface repository programme qui interagit directement avec la base de données
@Repository
public interface ProgrammeRepository extends JpaRepository<Programme, Long> {

        // Méthode pour récupérer un programme par son nom
        Programme findByName(String name);
}