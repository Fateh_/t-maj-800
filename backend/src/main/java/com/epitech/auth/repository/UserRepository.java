package com.epitech.auth.repository;

import com.epitech.auth.models.ApplicationUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

// Interface repository user qui interagit directement avec la base de données
@Repository
public interface UserRepository extends JpaRepository<ApplicationUser, Long> {

    // Interroger la table user en recherchant avec le username
    ApplicationUser findByUsername(String username);
}
