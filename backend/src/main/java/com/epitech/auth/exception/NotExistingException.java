package com.epitech.auth.exception;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

// Classe pour lever une exception en cas d'une requete qui n'existe pas
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NotExistingException extends RuntimeException {

    // Déclaration des attributs
    private String resourceName;
    private String resource;

    // Constructeur et implémentation de l'exception
    public NotExistingException(String resource,String resourceName) {
        super(String.format("%s with name %s doesnt exist", resource, resourceName));
        this.resourceName = resourceName;
        this.resource= resource;
    }

    /* Getters & Setters */

    public String getResourceName() {
        return resourceName;
    }

    public String getResource(){return resource;}
}

