package com.epitech.auth.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

// Classe pour lever une exception en cas d'une bad request
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {

        // Déclaration des attributs
        private String resourceName;
        private String resource;

        // Constructeur et implémentation de l'exception
        public BadRequestException(String resourceName,String resource) {
            super(String.format("%s %s already exists", resource,resourceName));
            this.resourceName = resourceName;
            this.resource = resource;
        }

        // Récupérer le nom de la ressource
        public String getResourceName() {
            return resourceName;
        }
}
