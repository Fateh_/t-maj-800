package com.epitech.auth.security;

// Déclaration des constantes utilisées pour la sécurité de l'application
public class SecurityConstants {
    public static final String SECRET = "SecretKeyToGenJWTs";
    public static final long EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/users/sign-up";
    public static final String GET_URL = "/users/all";
    public static final String DOC = "/swagger-ui.html";
}