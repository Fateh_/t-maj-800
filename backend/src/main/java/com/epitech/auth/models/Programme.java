package com.epitech.auth.models;

import com.sun.istack.NotNull;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.*;

// Déclaration de la table Programme
@Entity
@Table(name ="programme")
public class Programme {

    /* Déclaration des colonnes */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(unique=true)
    private String name;

    @NotNull
    private String type;

    @NotNull
    private Long annee;

    @NotNull
    private Long note;
    @NotNull
    private String owner;

    /* Getters & Setters  */
    
    public Long getId() {return this.id;}

    public Long getAnnee() {return this.annee;}
    public void setAnnee(Long annee){this.annee = annee;}

    public Long getNote(){return this.note;}
    public void setNote(Long note){this.note = note;}
    public String getName(){return this.name;}
    public void setName(String name){this.name = name;}

    public String getOwner(){return this.owner;}
    public void setOwner(String owner){this.owner = owner;}

    public String getType(){return this.type;}
    public void setType(String type){this.type = type;}
}
