package com.epitech.auth.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sun.istack.NotNull;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

// Déclaration de la table Diffusion
@Entity
@Table(name = "diffusion")
@JsonIgnoreProperties(allowGetters = true)
public class Diffusion {
    public Diffusion(){

    }

    /* Déclaration des colonnes */
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long id;

    @OneToOne
    @JoinColumn(name="programme_id")
    private Programme programme;

    @NotNull
    @Column
    public String date;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private ApplicationUser user;

    @Column
    public Long prix;

    /* Getters & Setters  */

    public Long getId(){return this.id;}

    public Programme getProgramme(){return this.programme;}
    public void setProgramme(Programme programme){this.programme = programme;}

    public String getDate(){return this.date;}
    public void setDate(String date){this.date = date;}

    public ApplicationUser getChaine(){return this.user;}
    public void setChaine(ApplicationUser chaine){this.user = chaine;}

    public Long getPrix(){return this.prix;}
    public void setPrix(Long prix){this.prix=prix;}
}
