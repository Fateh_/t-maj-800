package com.epitech.auth.user;

import com.epitech.auth.models.ApplicationUser;
import org.springframework.security.core.userdetails.User;
import com.epitech.auth.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import static java.util.Collections.emptyList;


// Classe service pour le user
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    // Déclaration de la variable repository du user
    private UserRepository userRepository;

    // Constructeur
    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    // Récupérer un user avec son username
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        ApplicationUser applicationUser = userRepository.findByUsername(username);
        if (applicationUser == null) {
            throw new UsernameNotFoundException(username);
        }
        return new User(applicationUser.getUsername(), applicationUser.getPassword(), emptyList());
    }
}
